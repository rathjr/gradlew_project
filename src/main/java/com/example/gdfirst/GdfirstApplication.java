package com.example.gdfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
public class GdfirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(GdfirstApplication.class, args);
    }
}
